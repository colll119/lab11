#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Load the dictionary of words from the given filename.
// Return a pointer to the array of strings.
// Sets the value of size to be the number of valid
// entries in the array (not the total array length).
char ** loadDictionary(char *filename, int *size)
{
	FILE *in = fopen(filename, "r");
	if (!in)
	{
	    perror("Can't open dictionary");
	    exit(1);
	}
	// TODO
    // Allocate memory for an array of strings (arr).
    int capacity = 10;
    char ** arr = malloc(capacity * sizeof(char*));
    int curr = 0; 
	// Read the dictionary line by line.
    char word[100];
    while(fgets(word,100,in) != NULL)
    {
        
        char *nl = strchr(word,'\n');
        if(nl)
        {
            *nl = '\0';
        }
        // Expand array if necessary (realloc)
        if(curr >= capacity)
        {
            capacity += 10;
            arr = realloc(arr, capacity * sizeof(char*));
        }
        // Allocate memory for the string (str).
        char * str = malloc((strlen(word)+1) * sizeof(char));
        // Copy each line into the string (use strcpy).
        strcpy(str, word);
        // Attach the string to the large array (assignment =).
        arr[curr] = str;
        curr++;
    } 
	// The size should be the number of entries in the array.
	*size = curr;
    fclose(in);
	// Return pointer to the array of strings.
	return arr;
}

// Search the dictionary for the target string
// Return the found string or NULL if not found.
char * searchDictionary(char *target, char **dictionary, int size)
{
    if (dictionary == NULL)
    return NULL;

	for (int i = 0; i < size; i++)
	{
	    if (strcmp(target, dictionary[i]) == 0)
	    {
            return dictionary[i];
	    }
	}
	return NULL;
}